apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/app-root: /webui
  name: reaper
spec:
  rules:
    - host: "{{ .Values.domain }}"
      http:
        paths:
          - backend:
              service:
                name: k8ssandra-dc1-reaper-service
                port:
                  number: 8080
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.domain }}"
      secretName: "{{ .Values.tls_wildcard_secret }}"
